http://stackoverflow.com/q/28447135/241294

Person asking about joining text files with awk and then performing aggregate queries. Sounds like a job better suited to sqlite:

    SQLite version 3.8.2 2013-12-06 14:53:30
    Enter ".help" for instructions
    Enter SQL statements terminated with a ";"
    sqlite> .separator ,
    sqlite> .import movie_data.csv  movie
    sqlite> .import user_data.csv user
    sqlite> .import rating_data.csv rating
    sqlite> .schema
    CREATE TABLE movie(
      "movie_id" TEXT,
      "movie_type" TEXT
    );
    CREATE TABLE user(
      "user_id" TEXT,
      "gender" TEXT,
      "occupation" TEXT
    );
    CREATE TABLE rating(
      "user_id" TEXT,
      "movie_id" TEXT,
      "rating" TEXT
    );
    sqlite> select r.movie_id, avg(r.rating) from rating r inner join user u on r.user_id = u.user_id where u.gender = 'F' group by r.movie_id having avg(r.rating) >= 3;
    1,10.0
    2,4.5
    4,6.5
    sqlite> select r.movie_id, avg(r.rating) from rating r
       ...> inner join user u on r.user_id = u.user_id
       ...> where u.gender = 'F'
       ...> group by r.movie_id
       ...> having avg(r.rating) >= 3;
    1,10.0
    2,4.5
    4,6.5
    sqlite> 
